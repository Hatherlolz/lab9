﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalculat_Click(object sender, EventArgs e)
        {
            double MinX1 = double.Parse(tbMinX1.Text);
            double MinX2 = double.Parse(tbMinX2.Text);
            double MaxX1 = double.Parse(tbMaxX1.Text);
            double MaxX2 = double.Parse(tbMaxX2.Text);
            double dX1 = double.Parse(tbdX1.Text);
            double dX2 = double.Parse(tbdX2.Text);

            gv.ColumnCount = (int)Math.Truncate((MaxX2 - MinX2) /dX2) + 1;
            gv.RowCount = (int)Math.Truncate((MaxX1 - MinX1) / dX1) + 1;

            for (int i= 0; i < gv.RowCount; i++)
            {
                gv.Columns[i].HeaderCell.Value = (MinX2 + i * dX2).ToString("0.000");
                gv.Columns[i].Width = 60;
            }

            //gv.AutoResizeColumns();
            //gv.AutoResizeRow();

            int column, row;
            double x1, x2, y;


            row = 0;
            x1 = MinX1;

            while(x1 < MinX1)
            {
                x2 = MinX2;
                column = 0;
                while(x2 < MinX2)
                {
                    y = Math.Pow(Math.Cos(3),3) * Math.Exp(x1 + 2 * x2 + 9 / 0.666);
                    gv.Rows[row].Cells[column].Value = y.ToString("0.000");
                    x2 += dX2;
                    column++;
                }

                x1 += dX1;
                row++;
            }
            tbMinX1.Text = "";
            tbMaxX1.Text = "";
            tbMinX2.Text = "";
            tbMaxX2.Text = "";
            tbdX1.Text = "";
            tbdX2.Text = "";

            gv.Rows.Clear();

            for(int col = 0; col < gv.ColumnCount; col++)
            {
                gv.Columns[col].HeaderCell.Value = "";
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити програму?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }
    }
}

﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MinX1 = new System.Windows.Forms.Label();
            this.MinX2 = new System.Windows.Forms.Label();
            this.MaxX1 = new System.Windows.Forms.Label();
            this.MaxX2 = new System.Windows.Forms.Label();
            this.dX1 = new System.Windows.Forms.Label();
            this.dX2 = new System.Windows.Forms.Label();
            this.tbMinX1 = new System.Windows.Forms.TextBox();
            this.tbMinX2 = new System.Windows.Forms.TextBox();
            this.tbMaxX1 = new System.Windows.Forms.TextBox();
            this.tbMaxX2 = new System.Windows.Forms.TextBox();
            this.tbdX1 = new System.Windows.Forms.TextBox();
            this.tbdX2 = new System.Windows.Forms.TextBox();
            this.btnCalculat = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.gv = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            this.SuspendLayout();
            // 
            // MinX1
            // 
            this.MinX1.AutoSize = true;
            this.MinX1.Location = new System.Drawing.Point(12, 28);
            this.MinX1.Name = "MinX1";
            this.MinX1.Size = new System.Drawing.Size(37, 13);
            this.MinX1.TabIndex = 1;
            this.MinX1.Text = "MinХ1";
            this.MinX1.Click += new System.EventHandler(this.label1_Click);
            // 
            // MinX2
            // 
            this.MinX2.AutoSize = true;
            this.MinX2.Location = new System.Drawing.Point(12, 72);
            this.MinX2.Name = "MinX2";
            this.MinX2.Size = new System.Drawing.Size(37, 13);
            this.MinX2.TabIndex = 2;
            this.MinX2.Text = "MinХ2";
            // 
            // MaxX1
            // 
            this.MaxX1.AutoSize = true;
            this.MaxX1.Location = new System.Drawing.Point(170, 28);
            this.MaxX1.Name = "MaxX1";
            this.MaxX1.Size = new System.Drawing.Size(40, 13);
            this.MaxX1.TabIndex = 3;
            this.MaxX1.Text = "MaxХ1";
            this.MaxX1.Click += new System.EventHandler(this.label3_Click);
            // 
            // MaxX2
            // 
            this.MaxX2.AutoSize = true;
            this.MaxX2.Location = new System.Drawing.Point(170, 72);
            this.MaxX2.Name = "MaxX2";
            this.MaxX2.Size = new System.Drawing.Size(40, 13);
            this.MaxX2.TabIndex = 4;
            this.MaxX2.Text = "MaxХ2";
            // 
            // dX1
            // 
            this.dX1.AutoSize = true;
            this.dX1.Location = new System.Drawing.Point(333, 31);
            this.dX1.Name = "dX1";
            this.dX1.Size = new System.Drawing.Size(26, 13);
            this.dX1.TabIndex = 5;
            this.dX1.Text = "dХ1";
            // 
            // dX2
            // 
            this.dX2.AutoSize = true;
            this.dX2.Location = new System.Drawing.Point(333, 72);
            this.dX2.Name = "dX2";
            this.dX2.Size = new System.Drawing.Size(26, 13);
            this.dX2.TabIndex = 6;
            this.dX2.Text = "dX2";
            // 
            // tbMinX1
            // 
            this.tbMinX1.Location = new System.Drawing.Point(71, 25);
            this.tbMinX1.Name = "tbMinX1";
            this.tbMinX1.Size = new System.Drawing.Size(69, 20);
            this.tbMinX1.TabIndex = 7;
            // 
            // tbMinX2
            // 
            this.tbMinX2.Location = new System.Drawing.Point(71, 65);
            this.tbMinX2.Name = "tbMinX2";
            this.tbMinX2.Size = new System.Drawing.Size(69, 20);
            this.tbMinX2.TabIndex = 8;
            // 
            // tbMaxX1
            // 
            this.tbMaxX1.Location = new System.Drawing.Point(232, 28);
            this.tbMaxX1.Name = "tbMaxX1";
            this.tbMaxX1.Size = new System.Drawing.Size(69, 20);
            this.tbMaxX1.TabIndex = 9;
            // 
            // tbMaxX2
            // 
            this.tbMaxX2.Location = new System.Drawing.Point(232, 69);
            this.tbMaxX2.Name = "tbMaxX2";
            this.tbMaxX2.Size = new System.Drawing.Size(69, 20);
            this.tbMaxX2.TabIndex = 10;
            // 
            // tbdX1
            // 
            this.tbdX1.Location = new System.Drawing.Point(382, 28);
            this.tbdX1.Name = "tbdX1";
            this.tbdX1.Size = new System.Drawing.Size(69, 20);
            this.tbdX1.TabIndex = 11;
            // 
            // tbdX2
            // 
            this.tbdX2.Location = new System.Drawing.Point(382, 69);
            this.tbdX2.Name = "tbdX2";
            this.tbdX2.Size = new System.Drawing.Size(69, 20);
            this.tbdX2.TabIndex = 12;
            // 
            // btnCalculat
            // 
            this.btnCalculat.Location = new System.Drawing.Point(474, 106);
            this.btnCalculat.Name = "btnCalculat";
            this.btnCalculat.Size = new System.Drawing.Size(75, 23);
            this.btnCalculat.TabIndex = 13;
            this.btnCalculat.Text = "Разрахувати";
            this.btnCalculat.UseVisualStyleBackColor = true;
            this.btnCalculat.Click += new System.EventHandler(this.btnCalculat_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(474, 155);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Очистити";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(474, 314);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "Вихід";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // gv
            // 
            this.gv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv.Location = new System.Drawing.Point(15, 106);
            this.gv.Name = "gv";
            this.gv.Size = new System.Drawing.Size(436, 231);
            this.gv.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 349);
            this.Controls.Add(this.gv);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCalculat);
            this.Controls.Add(this.tbdX2);
            this.Controls.Add(this.tbdX1);
            this.Controls.Add(this.tbMaxX2);
            this.Controls.Add(this.tbMaxX1);
            this.Controls.Add(this.tbMinX2);
            this.Controls.Add(this.tbMinX1);
            this.Controls.Add(this.dX2);
            this.Controls.Add(this.dX1);
            this.Controls.Add(this.MaxX2);
            this.Controls.Add(this.MaxX1);
            this.Controls.Add(this.MinX2);
            this.Controls.Add(this.MinX1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MinX1;
        private System.Windows.Forms.Label MinX2;
        private System.Windows.Forms.Label MaxX1;
        private System.Windows.Forms.Label MaxX2;
        private System.Windows.Forms.Label dX1;
        private System.Windows.Forms.Label dX2;
        private System.Windows.Forms.TextBox tbMinX1;
        private System.Windows.Forms.TextBox tbMinX2;
        private System.Windows.Forms.TextBox tbMaxX1;
        private System.Windows.Forms.TextBox tbMaxX2;
        private System.Windows.Forms.TextBox tbdX1;
        private System.Windows.Forms.TextBox tbdX2;
        private System.Windows.Forms.Button btnCalculat;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView gv;
    }
}

